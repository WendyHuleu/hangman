window.onload = function (){
    //Alphabet
    const alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];

    //Categorie des mots
    let categories;
    //Le jeu choisit une catégorie au hasard
    let chosenCategory;
    //Clavier
    var getHint;
    //Mot selectioné
    let word;
    //Mot à deviné
    let guess;
    //Mot à deviné stoké
    let geusses = [];
    // Nombres de vie (10)
    let lives;
    // Compteur
    let counter;
    //Nombre d'espaces dans le mot '-'
    let space;

    //constente du jeu, Vie, selection de la cathégorie, apparision du clavier,indice
    const showlives = document.getElementById("mylives");
    const showCatagory = document.getElementById("scatagory");
    var getHint = document.getElementById("hint");
    const showClue = document.getElementById("clue");

    //Clavier virtuel
    const buttons = function () {
        myButtons = document.getElementById('buttons');
        letters = document.createElement('ul');

        for (let i = 0; i < alphabet.length; i++) {
            letters.id = 'alphabet';
            list = document.createElement('li');
            list.id = 'letter';
            list.innerHTML = alphabet[i];
            check();
            myButtons.appendChild(letters);
            letters.appendChild(list);
        }
    };

    //Message de selection de cathégorie
    const selectCat = function () {
        if (chosenCategory === categories[0]) {
            catagoryName.innerHTML = "La cathégorie choisi pour cette partie est : JEUX VIDEO";
        } else if (chosenCategory === categories[1]) {
            catagoryName.innerHTML = "La cathégorie choisi pour cette partie est : FILMS";
        } else if (chosenCategory === categories[2]) {
            catagoryName.innerHTML = "La cathégorie choisi pour cette partie est : VILLES";
        }
    };


    //Espace du mot à trouver
    let result = function () {
         wordHolder = document.getElementById('hold');
         correct = document.createElement('ul');

        for (let i = 0; i < word.length; i++) {
            correct.setAttribute('id', 'my-word');
            guess = document.createElement('li');
            guess.setAttribute('class', 'guess');
            if (word[i] === "-") {
                guess.innerHTML = "-";
                space = 1;
            } else {
                guess.innerHTML = "_";
            }

            geusses.push(guess);
            wordHolder.appendChild(correct);
            correct.appendChild(guess);
        }
    }

    //Voir ses vies
    let comments = function () {
        showlives.innerHTML = "Vous avez " + lives + " vies.";
        if (lives < 1) {
            showlives.innerHTML = "Vous avez perdu"
        }
        for (let i = 0; i < geusses.length; i++) {
            if (counter + space === geusses.length) {
                showlives.innerHTML = "Vous avez gagné"
            }
        }
    }


    //Animation du hangman

    const animate = function () {
        const drawMe = lives;
        drawArray[drawMe]();
    };

    let canvas = function () {
        myHangman = document.getElementById("hangman");
        context = myHangman.getContext('2d');
        context.beginPath();
        context.strokeStyle = "#000";
        context.lineWidth = 2;
    };

    //Emplacement du Hangman
    head = function () {
        myHangman = document.getElementById("hangman");
        context = myHangman.getContext('2d');
        context.beginPath();
        context.arc(60, 25, 10, 0, Math.PI * 2, true);
        context.stroke();
    }

    draw = function ($pathFromx, $pathFromy, $pathTox, $pathToy) {

        context.moveTo($pathFromx, $pathFromy);
        context.lineTo($pathTox, $pathToy);
        context.stroke();
    }

    let frame1 = function () {
        draw(0, 150, 150, 150);
    };

    let frame2 = function () {
        draw(10, 0, 10, 600);
    };

    let frame3 = function () {
        draw(0, 5, 70, 5);
    };

    let frame4 = function () {
        draw(60, 5, 60, 15);
    };

    let torso = function () {
        draw(60, 36, 60, 70);
    };

    let rightArm = function () {
        draw(60, 46, 20, 50);
    };

    let rightLeg = function () {
        draw(60, 46, 20, 50);
    };

    let leftArm = function () {
        draw(60, 70, 100, 100);
    };

    let leftLeg = function () {
        draw(60, 70, 100, 100);
    };

    drawArray = [rightLeg, leftLeg, rightArm, leftArm, torso, head, frame4, frame3, frame2, frame1];

    let check = function () {
        list.onclick = function () {
            const geuss = (this.innerHTML);
            this.setAttribute("class", "active");
            this.onclick = null;
            for (let i = 0; i < word.length; i++) {
                if (word[i] === geuss) {
                    geusses[i].innerHTML = guess;
                    counter += 1;
                }
            }
            const j = (word.indexOf(geuss));
            if (j === -1) {
                lives -= 1;
                comments();
                animate();
            } else {
                comments()
            }
        }
    }

    //Mot à deviner de chaque catégories
    let play = function () {
        categories = [
            ["league of legends", "world of warcraft", "litle nightmare","fortnine","Zelda"],
            ["predador", "alien", "avenger","spiderman","la list de shidler"],
            ["clermont ferrand", "paris", "lyon","lile"]
        ];

        chosenCategory = categories[Math.floor(Math.random() * categories.length)];
        word = chosenCategory[Math.floor(Math.random() * chosenCategory.length)];
        word = word.replace(/\s/g, "-");
        console.log(word);
        buttons();

        geusses = [ ];
        lives = 10;
        counter = 0;
        space = 0;
        result();
        comments();
        selectCat();
        canvas();
    }

    play();

    //Gestion des indices -Certains ne marche pas-
    hint.onclick = function () {

    hints = [
        ["moba", "mmorpg", "Cauchemard"],
        ["science-fiction survie jungle", "science-fiction horreur", "Super-héros","Batle Royal","Triforce"],
        ["pierre-noire", "capital", "animal"]
    ];

    const catagoryIndex = categories.indexOf(chosenCategory);
    const hintIndex = chosenCategory.indexOf(word);
    showClue.innerHTML = "Indice: " + hints [catagoryIndex][hintIndex];
};

//Bouton rejouer
    document.getElementById('reset').onclick = function() {
        correct.parentNode.removeChild(correct);
        letters.parentNode.removeChild(letters);
        showClue.innerHTML = "";
        context.clearRect(0, 0, 400, 400);
        play();
    }
}


